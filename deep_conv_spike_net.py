import torch
import torch.nn as nn
import numpy as np
import time
import matplotlib
import matplotlib.pyplot as plt

import torch.nn.functional as F

from torch_SNN.base import waveform,time_stepped_object,net_base
from torch_SNN.utilities import make_waveform_table,raster_plot
from torch_SNN.layers import error_layer,poisson_layer,spike_layer,fully_connected_transformer,conv_transformer
from torch_SNN.plasticity_rules import super_spike_rule

class deep_conv_spike_net(net_base):
      
    def __init__(self,batch_size = 32):
        super(deep_conv_spike_net, self).__init__()

	self.poisson_input = poisson_layer((1,28,28),batch_size = batch_size,rate = 40,deterministic=True)		   

        self.conv1 = spike_layer((64,28,28),conv_transformer(kernel_size = 5,stride = 1,padding = 2))
        self.conv2 = spike_layer((96,14,14),conv_transformer(kernel_size = 5,stride = 1,padding = 2))
        self.conv3 = spike_layer((128,7,7),conv_transformer(kernel_size = 5,stride = 1,padding = 2))                

	self.make_temporal_obj_list()
	
	
    def forward(self):
	src_spikes = self.poisson_input()

	conv1_spikes,conv1_vmem = self.conv1(src_spikes)
        conv1_spikes_pooled = F.max_pool2d(conv1_spikes.float(),2)
	conv2_spikes,conv2_vmem = self.conv2(conv1_spikes_pooled)
        conv2_spikes_pooled = F.max_pool2d(conv2_spikes.float(),2)
	conv3_spikes,conv3_vmem = self.conv3(conv2_spikes_pooled)                
        conv3_spikes_pooled = F.max_pool2d(conv3_spikes.float(),2)
        
	
	self.advance_time_step()
	return conv3_spikes_pooled



time_step = 0.001
run_duration = 1.0

n_steps = int(run_duration / time_step)

net = deep_conv_spike_net()
net.set_time_step(time_step)

net.cuda()

#run once to initialize waveforms using the dynamic allocation based on input sizes
net.forward()
net.reset()

#make waveform table
all_wfs = make_waveform_table(net)

all_wfs['conv1:vmem'].set_save_depth(n_steps,[0,slice(0,1),1,3])

n_trials = 1
for trial in range(n_trials):
    t1 = time.time()
    net.reset()
    for i in range(n_steps):
	net()
    trial_time = time.time() - t1
    print 'trial ' + repr(trial) + ' took ' + repr(trial_time)


print all_wfs['conv1:vmem'].wf.size()
plt.plot(np.arange(n_steps)*time_step,all_wfs['conv1:vmem'].wf.cpu().numpy())    
plt.show()
